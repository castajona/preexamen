static void Main(string[] args)
        {//Se desea introducir por teclado una tabla de 4 filas por 5 columnas de elementos enteros.
            //Escribir un programa que tras la lectura los visualice en pantalla.
            int F = 0;
            int C = 0;

            int[,] MAT = new int[6, 5];





            // GENERO LOS NÚMEROS
            for (F = 1; F <= 5; F++)
            {
                for (C = 1; C <= 4; C++)
                {
                    Console.Write("Ingrese valor: ");
                    int X = Convert.ToInt32(Console.ReadLine());
                    MAT[F, C] = X;
                }
            }

            // SALIDA
            for (F = 1; F <= 5; F++)
            {
                for (C = 1; C <= 4; C++)
                {
                    // APROVECHAMOS CICLOS Y GENERAMOS POSICIÓN EN PANTALLA
                    // PARA COLUMNA Y FILA
                    Console.SetCursorPosition(C * 15, F + 21);
                    Console.Write(MAT[F, C]);
                }

            }
            Console.WriteLine();
            Console.Write("Pulse una Tecla:");
            Console.ReadLine();
        }